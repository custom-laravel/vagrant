tatsuki_kubota/vagrant
---

開発環境構築用パッケージ

## ■ About
### ・想定動作環境
* Windows 10 (64bit)
* RAM 4GB～
* No consideration proxy server

### ・必要ソフトウェア
* Oracle Virtual Box (v5.2.12)
* Vagrant (v2.1.1)

### ・使用ソフトウェア (Include)
* CentOS (v7.X)
* docker (v18.05.0)
* docker-compose (v1.14.0)
* nginx (docker 1.14-alpine)
* php (docker 7.1-fpm-alpine・7.1-cli-alpine)
* mysql (docker 5.7.19)  





## ■ Install
### 技術書典5 ご報告  
「ざっくり知るLaravel」のご購入または、閲覧ありがとうございました。  
初出展ながら、この本を目指してお越しいただいた方がいた事に感銘を受けており、未だに忘れられません。  
とてもたくさんの方に手に取って頂き、お声を掛けていただき、本当に楽しかったです。  
私としては、皆様にLaravelを知ってもらえたら、嬉しい限りです。  
次回も頑張りますので、温かいご支援の程よろしくお願い致します。  
よろしければ、アンケートにご協力いただけると幸いです。 (2018/12/01締め切り)  
https://goo.gl/forms/gWSRQ0IaAVZGTzzI2  

### 【注意】  
・かなりのネットワーク通信が発生します。  
・安定したネットワーク回線をご利用ください。  

### 1. Oracle Virtual Box インストール
以下のサイトよりダウンロードし、インストール。  
(※ ダウンロードファイルのセキュリティスキャンを忘れない事)  
https://www.virtualbox.org/wiki/Downloads  

### 2. Vagrant インストール
以下のサイトよりダウンロードし、インストール。  
(※ ダウンロードファイルのセキュリティスキャンを忘れない事)  
https://www.vagrantup.com/  

### 3. Vagrant plugin追加
以下のコマンドをコマンドプロンプト or ターミナルより実行する。  
```
vagrant plugin install vagrant-vbguest
vagrant vbguest
```

### 4. `vagrant`フォルダ作成
以下のコマンドをコマンドプロンプト or ターミナルより実行する。  
(※ コマンドプロンプト or ターミナルは、手順「6」でも引き続き使用する為、ウィンドウを閉じない)
- Windows (コマンドプロンプト・管理者権限での実行推奨)
```
mkdir %HOMEPATH%/vagrant
cd %HOMEPATH%/vagrant
```

- MacOSX (ターミナル)
```
mkdir $HOME/vagrant
cd $HOME/vagrant
```

### 5. 本リポジトリ(フォルダ)をダウンロードし、`vagrant`フォルダ内に中身を解凍する。
https://gitlab.com/custom-laravel/vagrant/-/archive/master/vagrant-master.zip

### 6. vagrant起動  
以下のコマンドをコマンドプロンプト or ターミナルより実行する。(Windows・MacOSX共通)  
(※ 初回はインストールに時間が掛かる為注意)  
```
vagrant up
```

### 7. vagrant停止  
以下のコマンドをコマンドプロンプト or ターミナルより実行する。 (Windows・MacOSX共通)  
```
vagrant halt
```

### 8. (MacOSXのみ) ファイル権限を全て777にする
```
vagrant halt
cd $HOME/
sudo chmod 777 -R ./vagrant
vagrant up
```

### 9. (MacOSXのみ) クエリをphpMyAdminにて実行
```
cd $HOME/vagrant/server/migrate/shell_list/20181001
open .
```
・「 http://192.168.33.10:13306/server_sql.php?lang=ja 」を開く。
・開いたFinderにある「laravel.sql」をドラッグ＆ドロップでSQLを実行する





## ■ トラブルシューティング
### ・「 http://192.168.33.10 」が開けない  
→ 安定したネットワーク環境での実行をお願い致します。  
→ ``vagrant reload``を実行をお願い致します。  

### ・それでも解決しないようであれば...  
→ 「 https://gitlab.com/custom-laravel/vagrant/issues/new 」にてご報告をお待ちしております。  
→ 回答までしばらくお待ちください。  





## ■ Info
### サービスアクセス
* WEBサーバ  
https://192.168.33.10
* DB (phpmyadmin)  
http://192.168.33.10:13306
* Log.io (ログファイルの関係上、vagrant起動後に再起動してから利用可能)  
http://192.168.33.10:28778

### 認証 (docker-compose.ymlのコメントアウト無効解除により認証可能)
* DB (mysql)  
  port : 3306  
  default_database : work  
  user : root  
  password : password    
* Log.io  
  id : admin  
  pasword : password  

### ログ
* `./server/log`に格納
