#!/bin/sh
# --------------------------------------------------------------------------------
# setup batch
# --------------------------------------------------------------------------------

# ----- config -----
GIT_EMAIL="chamge_your_email"
GIT_USERNAME="change_your_username"
GIT_PASSWORD="change_your_password"
GIT_REPOSITORY_SERVER="https://gitlab.com/custom-laravel/server.git"
GIT_REPOSITORY_APPLICATION="https://gitlab.com/custom-laravel/application.git"

# ----- Git config -----
# /usr/local/bin/git config --global user.email "${GIT_EMAIL}"
# /usr/local/bin/git config --global user.name "${GIT_USERNAME}"
/usr/local/bin/git config --global http.sslVerify false
# cat > ~/.netrc <<EOF
# machine gitlab.com
# login ${GIT_EMAIL}
# password ${GIT_PASSWORD}
# EOF
# cat > /home/vagrant/.netrc <<EOF
# machine gitlab.com
# login ${GIT_EMAIL}
# password ${GIT_PASSWORD}
# EOF

# ----- encrypt git profile (NEED LOT OF TIME) -----
# cat > ~/gpg-key.conf <<EOF
# Key-Type: RSA
# Key-Length: 3072
# Subkey-Type: RSA
# Subkey-Length: 3072
# Subkey-Usage: encrypt
# Name-Real: ${GIT_USERNAME}
# Name-Email: ${GIT_EMAIL}
# Expire-Date: 0
# Passphrase: ${GIT_PASSWORD}
# %commit
# %echo done
# EOF
# gpg --gen-key --batch ~/gpg-key.conf
# ~/rm .netrc

# ----- Make server -----
/usr/local/bin/git -C /server init
/usr/local/bin/git -C /server remote add origin ${GIT_REPOSITORY_SERVER}
/usr/local/bin/git -C /server pull origin master

# ----- Make server/application -----
mkdir /server/application
/usr/local/bin/git -C /server/application init
/usr/local/bin/git -C /server/application remote add origin ${GIT_REPOSITORY_APPLICATION}
/usr/local/bin/git -C /server/application pull origin master

# ----- Make server/application/public/strage (symbolic link) -----
ln -s /server/application/storage/app/public /server/application/public/storage

# ----- start up -----
/usr/local/bin/docker-compose -f /server/docker-compose.yml up -d --build

# ----- php composer install -----
/usr/bin/docker exec -i php-fpm composer install

exit 0;
