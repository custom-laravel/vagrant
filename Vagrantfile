Vagrant.configure("2") do |config|
  config.vm.box = "centos/7"
  config.vm.box_check_update = false
  config.vm.hostname = "centos"
  config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"
  config.vm.network "private_network", ip: "192.168.33.10"
  # config.vm.network "public_network"
  config.vm.synced_folder "./server", "/server"

  config.vm.provider "virtualbox" do |vb|
    vb.name = "centos"
    vb.cpus = 2
    vb.gui = false
    vb.memory = "2096"
  end

  # First Install
  config.vm.provision "shell", inline: <<-SHELL
    # ----- Config -----
    GIT_VERSION="2.12.5"
    # ----- Tool -----
    sudo yum -y install vim curl-devel expat-devel gettext-devel openssl-devel perl-devel zlib-devel autoconf asciidoc xmlto docbook2X make gcc expect
    # ----- Git -----
    sudo yum -y remove git
    cd /usr/local/src/
    sudo curl -o git-${GIT_VERSION}.tar.gz -L https://www.kernel.org/pub/software/scm/git/git-${GIT_VERSION}.tar.gz
    sudo tar xzvf git-${GIT_VERSION}.tar.gz
    sudo rm git-${GIT_VERSION}.tar.gz
    cd git-${GIT_VERSION}/
    sudo make prefix=/usr/local all
    sudo make prefix=/usr/local install
    sudo chmod +x /usr/local/bin/git
    # ----- Docker -----
    sudo curl -sSL https://get.docker.com/ | sh
    sudo systemctl enable docker
    sudo gpasswd -a vagrant docker
    sudo systemctl start docker
    # ----- Docker Compose -----
    sudo curl -L https://github.com/docker/compose/releases/download/1.14.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
  SHELL

  # Always run
  config.vm.provision "shell", run: "always", inline: <<-SHELL
    if [ -e /server/docker-compose.yml ]; then
      ## START UP --------------------------------------------------
      /usr/local/bin/docker-compose -f /server/docker-compose.yml down
      /usr/local/bin/docker-compose -f /server/docker-compose.yml up -d --build
      /bin/sh /server/migrate.sh
    else
      ## SET UP --------------------------------------------------
      mv /server/setup.sh ~/setup.sh
      /bin/sh ~/setup.sh
      /bin/sh /server/migrate.sh
    fi
  SHELL
end
