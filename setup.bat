@ECHO off
REM --------------------------------------------------------------------------------
REM Vagrant作成
REM --------------------------------------------------------------------------------

REM vagrant vbguest 導入・更新
vagrant plugin install vagrant-vbguest
vagrant vbguest

cd /d %~dp0
call vagrant up --provision

ECHO "Vagrant set-up done."